FROM ubuntu:16.04

# Ignore APT warnings about not having a TTY
ENV DEBIAN_FRONTEND noninteractive

# Ensure UTF-8 locale
RUN apt-get update -qq && \
    apt-get install -y locales && \
    rm -rf /var/lib/apt/lists/*
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && locale-gen \
    && dpkg-reconfigure locales

RUN apt-get update -qq && apt-get upgrade -y  && rm -rf /var/lib/apt/lists/*

# Install dependencies.
RUN apt-get update -qq \
    && apt-get install -y wget git curl build-essential g++ gcc make \
    && rm -rf /var/lib/apt/lists/*

# Install Erlang Solutions.
RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb \
    && dpkg -i erlang-solutions_1.0_all.deb

# Install all of Erlang/OTP.
RUN apt-get update -qq \
    && apt-get install -y esl-erlang elixir \
    && rm -rf /var/lib/apt/lists/*

# Install nodejs
RUN curl -sL https://deb.nodesource.com/setup_9.x | bash - \
    && apt-get install -y nodejs \
    && rm -rf /var/lib/apt/lists/*

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo 'deb https://dl.yarnpkg.com/debian/ stable main' \
    > /etc/apt/sources.list.d/yarn.list \
  && apt-get update && apt-get install yarn \
  && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

RUN yes | mix local.hex
COPY mix.* ./
RUN mix local.rebar
RUN mix deps.get
